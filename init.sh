#!/bin/bash

# Usage:
# - Create a new directory and cd into it
# - depending on the usage you might edit GIT_REMOTE and SVN_SERVER
# - run this script
# - run the update script

if [ -d ".git" ]; then
    echo "Error: git is already defined in the current directory"
    exit
fi

if [ ! -z "$(ls -A -- "$(pwd)")" ]; then
    echo "Error: the current directory is not empty"
    exit
fi

GIT_REMOTE="git@gitlab.com:scribus/scribus16.git"
SVN_SERVER="svn://scribus.net"

# space separated names of branches in SVN
# get the list of the available branches with 
# svn ls "$SVN_SERVER/branches"
SVN_BRANCH="Version15x"

git svn clone --authors-file=$(dirname $0)/svn-authors.txt $SVN_SERVER/branches/$SVN_BRANCH/Scribus .

git remote add origin $GIT_REMOTE
