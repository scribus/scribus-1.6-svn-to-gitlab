# Scribus 1.6 - SVN to Gitlab

This repository contains the scripts for keeping the Gitlab Scribus 1.6 repository in sync with the the Scribus SVN.

The 1.5.x SVN branch is currently used.

The SVN repository contains an additional folder "Scribus" at the
project root. This folder is skipped during conversion, so that the
programm sources appear in the repository root.

The `init.sh` script configures an empty Git repository to receive the SVN commits.

The `update.sh` script pulls the new commits from SVN and pushes the result to the _origin_ Git remote repository on Gitlab.

## Requirements

- bash
- git
- subversion
- git-svn

## How to setup

First, you will have to clone this repository. We suppose you are cloning it into `~/src`, but you can pick any directory you prefer.

    cd ~/src
    git clone https://gitlab.com:scribus/scribus-1.6-svn-to-gitlab.git

Then create a local repository called `scribus16-for-sync` (or any other name you see fit) that you will use to contain the sync.

```bash
cd ~/src
git init scribus16-for-sync
```

Finally, use the scripts from the `scribus-1.6-svn-to-gitlab` repository to setup the local repository so that it can be used for syncing the Scribus repositories. (it will take some time)

```bash
cd ~/src/scribus16-for-sync
~/src/scribus-1.6-svn-to-gitlab/init.sh
```

## How to sync

Now, each time you want to update the `scribus/scribus16` you go into the `scribus16-for-sync` repository and run the update script:

```bash
cd ~/src/scribus16-for-sync
~/src/scribus-1.6-svn-to-gitlab/update.sh
```

If you get the error:

```
error: failed to push some refs to 'gitlab.com:scribus/scribus16.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

## Caveat

All SVN commiters need to be listed in `svn_authors.txt`. A missing
author will lead to an error:

``
Author: <username> not defined in /path/to/svn_authors.txt file
``

In this case, add the new commiter to the `svn_authors.txt` file with
the following format:

```
username = Full Name <email@address.xzy>
```

and re-run the import.

## Trouble shooting

### Pushed a non svn commit

If you have pushed a commit that does not come from SVN and it's still the latest commit:

From an up to date (with the wrong commit) local copy of the Scribus Github code::

```sh
git reset --keep HEAD~
git log
git push --force-with-lease upstream master
```

## External references

* github to svn and svn to github sync scripts using another git repo : http://zone.spip.org/trac/spip-zone/browser/_outils_/svn2git/trunk
